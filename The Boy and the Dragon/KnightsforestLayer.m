//
//  KnightsforestLayer.m
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 5/31/14.
//  Copyright 2014 Muvu Media. All rights reserved.
//

#import "KnightsforestLayer.h"
#import "GiantshillLayer.h"


@interface KnightsforestLayer ()

@property (nonatomic, strong) CCSprite *butterflySprite;
@property (nonatomic, strong) CCSprite *chickySprite;
@property (nonatomic, strong) CCSprite *deerSprite;
@property (nonatomic, strong) CCSprite *signSprite;

@property (nonatomic, strong) CCMenuItemSprite *butterfly;
@property (nonatomic, strong) CCMenuItemSprite *chicky;
@property (nonatomic, strong) CCMenuItemSprite *deer;
@property (nonatomic, strong) CCMenuItemSprite *sign;

@property (nonatomic, strong) CCSprite *forestBoy;
@property (nonatomic, strong) CCSprite *forestKnight;
@property (nonatomic, strong) CCSprite *forestKnightArm;
@property (nonatomic, strong) CCSprite *forestKnightShield;
@property (nonatomic, strong) CCSprite *forestKnightLance;
@property (nonatomic, strong) CCSprite *forestKnightHorse;

@property (nonatomic, strong) NSArray *boyFramesArray;
@property (nonatomic, strong) NSArray *knightFramesArray;

@property (nonatomic, strong) NSMutableArray *boyFrames1;
@property (nonatomic, strong) NSMutableArray *boyFrames2;
@property (nonatomic, strong) NSMutableArray *boyFrames3;
@property (nonatomic, strong) NSMutableArray *boyFrames4;
@property (nonatomic, strong) NSMutableArray *knightFrames1;
@property (nonatomic, strong) NSMutableArray *knightFrames2;
@property (nonatomic, strong) NSMutableArray *knightShieldFrames2;
@property (nonatomic, strong) NSMutableArray *knightArmFrames2;
@property (nonatomic, strong) NSMutableArray *knightFrames3;
@property (nonatomic, strong) NSMutableArray *knightFrames4;

@property (nonatomic, strong) CCAnimation *boyAnimation1;
@property (nonatomic, strong) CCAnimation *boyAnimation2;
@property (nonatomic, strong) CCAnimation *boyAnimation3;
@property (nonatomic, strong) CCAnimation *boyAnimation4;
@property (nonatomic, strong) CCAnimation *knightAnimation1;
@property (nonatomic, strong) CCAnimation *knightAnimation2;
@property (nonatomic, strong) CCAnimation *knightArmAnimation2;
@property (nonatomic, strong) CCAnimation *knightShieldAnimation2;
@property (nonatomic, strong) CCAnimation *knightAnimation3;
@property (nonatomic, strong) CCAnimation *knightAnimation4;

@end

BOOL deerLeft;

@implementation KnightsforestLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	KnightsforestLayer *layer = [KnightsforestLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

- (id)init {
    if( (self=[super init]) ) {
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"deer.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"butterfly.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"chicky.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"sign.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"forestABoy.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"forestAKnight.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"forestBKnight.plist"];
        
        NSArray *sentenceBlock1 = [NSArray arrayWithObjects:
                                   @"When he got to the forest",
                                   @"the boy met a knight,",
                                   @"who was riding a horse",
                                   @"and all dressed in white.",
                                   nil];
        NSArray *sentenceBlock2 = [NSArray arrayWithObjects:
                                   @"The knight tried to lift",
                                   @"a big metal shield,",
                                   @"but it was too heavy",
                                   @"and he dropped it on the field.",
                                   nil];
        NSArray *sentenceBlock3 = [NSArray arrayWithObjects:
                                   @"The knight kicked his horse",
                                   @"to move him ahead",
                                   @"but the horse got mad",
                                   @"and kicked the knight off instead.",
                                   nil];
        NSArray *sentenceBlock4 = [NSArray arrayWithObjects:
                                   @"The boy kept going",
                                   @"and he walked right around",
                                   @"the poor white knight",
                                   @"who was stuck upside down.",
                                   nil];
        
        self.sentenceBlocks = [NSMutableArray arrayWithObjects:sentenceBlock1, sentenceBlock2, sentenceBlock3, sentenceBlock4, nil];
        
        [self setUpAnimations];
        [self setUpInitialFrame];
        
    }
    return self;
}

-(void)setUpAnimations {
    
    self.boyFrames1 = [NSMutableArray array];
    
    self.knightFrames1 = [NSMutableArray array];
    self.knightFrames2 = [NSMutableArray array];
    
    for (int i = 2; i <= 126; i++) {
        [self.boyFrames1 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"knightsforest1boy%d.png", i]]];
        [self.knightFrames1 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"knightsforest1knight%d.png", i]]];
    }
    for (int i = 2; i <= 163; i++) {
        [self.knightArmFrames2 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"KnightsForest2ArmBehindHorse%d.png", i]]];
        [self.knightFrames2 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"KnightsForest2KnightInfrontofHorse%d.png", i]]];
        [self.knightShieldFrames2 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"KnightsForest2Shield%d.png", i]]];
    }
    self.boyAnimation1 = [CCAnimation animationWithSpriteFrames:self.boyFrames1 delay:0.025f];
    self.knightAnimation1 = [CCAnimation animationWithSpriteFrames:self.knightFrames1 delay:0.025f];
    self.knightArmAnimation2 = [CCAnimation animationWithSpriteFrames:self.knightArmFrames2 delay:0.025f];
    self.knightAnimation2 = [CCAnimation animationWithSpriteFrames:self.knightFrames2 delay:0.025f];
    self.knightShieldAnimation2 = [CCAnimation animationWithSpriteFrames:self.knightShieldFrames2 delay:0.025f];
}

- (void) setUpInitialFrame {
    self.background = [CCSprite spriteWithFile:@"forestBG.png"];
    self.background.anchorPoint = CGPointMake(0, 0);
    
    self.sign = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"sign1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"sign1.png"] target:self selector:@selector(signTapped   )];
    [self.sign setPosition:ccp(225, 415)];
    [self.sign setOpacity:0];
    
    self.signSprite = [CCSprite spriteWithSpriteFrameName:@"sign1.png"];
    [self.signSprite setPosition:ccp(225, 415)];
    
    self.deer = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"deer1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"deer1.png"] target:self selector:@selector(deerTapped)];
    [self.deer setPosition:ccp(465, 337)];
    [self.deer setOpacity:0];
    
    self.deerSprite = [CCSprite spriteWithSpriteFrameName:@"deer1.png"];
    [self.deerSprite setPosition:ccp(465, 337)];
    deerLeft = YES;
    
    self.butterfly = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"butterfly1_result.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"butterfly1_result.png"] target:self selector:@selector(butterflyTapped)];
    [self.butterfly setPosition:ccp(140, 130)];
    [self.butterfly setOpacity:0];
    
    self.butterflySprite = [CCSprite spriteWithSpriteFrameName:@"butterfly1_result.png"];
    [self.butterflySprite setPosition:ccp(140, 130)];
    
    self.chicky = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"chicky1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"chicky1.png"] target:self selector:@selector(chickyTapped)];
    [self.chicky setPosition:ccp(900, 565)];
    [self.chicky setOpacity:0];
    
    self.chickySprite = [CCSprite spriteWithSpriteFrameName:@"chicky1.png"];
    [self.chickySprite setPosition:ccp(900, 565)];
    
    self.clickableMenu = [CCMenu menuWithItems:self.deer, self.butterfly, self.chicky, self.sign, nil];
    self.clickableMenu.position = CGPointZero;
    
    self.forestBoy = [CCSprite spriteWithSpriteFrameName:@"knightsforest1boy1.png"];
    [self.forestBoy setPosition:ccp(1024/2, 768/2)];
    self.forestKnight = [CCSprite spriteWithSpriteFrameName:@"knightsforest1knight1.png"];
    [self.forestKnight setPosition:ccp(1024/2, 768/2)];
    self.forestKnightHorse = [CCSprite spriteWithFile:@"KnightsForest2Horse.png"];
    [self.forestKnightHorse setPosition:ccp(1024/2, 768/2)];
    self.forestKnightLance = [CCSprite spriteWithFile:@"KnightsForest2Lance.png"];
    [self.forestKnightLance setPosition:ccp(1024/2, 768/2)];
    self.forestKnightArm = [CCSprite spriteWithSpriteFrameName:@"KnightsForest2ArmBehindHorse1.png"];
    [self.forestKnightArm setPosition:ccp(1024/2, 768/2)];
    self.forestKnightShield = [CCSprite spriteWithSpriteFrameName:@"KnightsForest2Shield1.png"];
    [self.forestKnightShield setPosition:ccp(1024/2, 768/2)];
    [self.forestKnightShield setOpacity:0];
    [self.forestKnightArm setOpacity:0];
    [self.forestKnightHorse setOpacity:0];
    [self.forestKnightLance setOpacity:0];
    
    [self addChild:self.clickableMenu z:1];
    [self addChild:self.deerSprite z:1];
    [self addChild:self.butterflySprite z:1];
    [self addChild:self.chickySprite z:1];
    [self addChild:self.signSprite z:1];
    [self addChild:self.forestBoy z:1];
    [self addChild:self.forestKnightLance z:1];
    [self addChild:self.forestKnightArm z:1];
    [self addChild:self.forestKnight z:1];
    [self addChild:self.forestKnightHorse z:1];
    [self addChild:self.forestKnightShield z:1];
    
    
    [super setupInitialFrame];
}

- (void) nextSceneTapped {
    [super nextSceneTapped];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GiantshillLayer scene] withColor:ccWHITE]];
    
}

- (void) signTapped {
    NSLog(@"signTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 50; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"sign%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.signSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

- (void) deerTapped {
    NSLog(@"deerTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    if (deerLeft) {
        for (int i = 2; i <= 46; i++) {
                [frames addObject:
                 [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
                  [NSString stringWithFormat:@"deer%d.png", i]]];
        }
    }
    else {
        for (int i = 2; i <= 34; i++) {
            [frames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"deer2_%d.png", i]]];
        }

    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.deerSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
    deerLeft = !deerLeft;
}

-(void) butterflyTapped {
    NSLog(@"butterflyTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 70; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"butterfly%d_result.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.butterflySprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

-(void) chickyTapped {
    NSLog(@"chickyTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 43; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"chicky%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.chickySprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

- (void) runAnimation: (int) animationNumber {
    
    if (animationNumber == 1) {
        [self.forestBoy runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.boyAnimation1], nil]];
        [self.forestKnight runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.knightAnimation1], nil]];
    }
    else if (animationNumber == 2) {
        [self.forestKnightLance setOpacity:255];
        [self.forestKnightShield setOpacity:255];
        [self.forestKnightArm setOpacity:255];
        [self.forestKnightHorse setOpacity:255];
        [self.forestKnightShield setOpacity:255];
        [self.forestKnightShield runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.knightShieldAnimation2], nil]];
        [self.forestKnightArm runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.knightArmAnimation2], nil]];
        [self.forestKnight runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.knightAnimation2], nil]];
    }
}

@end
