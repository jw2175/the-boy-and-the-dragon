//
//  AtticLayer.m
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 5/28/14.
//  Copyright 2014 Muvu Media. All rights reserved.
//

#import "AtticLayer.h"
#import "FarawaylandLayer.h"

@interface AtticLayer ()

@property (nonatomic, strong) CCSprite *bearSprite;
@property (nonatomic, strong) CCSprite *mouseSprite;
@property (nonatomic, strong) CCSprite *teacupSprite;
@property (nonatomic, strong) CCSprite *clockSprite;
@property (nonatomic, strong) CCSprite *spiderSprite;
@property (nonatomic, strong) CCSprite *lampSprite;

@property (nonatomic, strong) CCMenuItemSprite *bear;
@property (nonatomic, strong) CCMenuItemSprite *mouse;
@property (nonatomic, strong) CCMenuItemSprite *teacup;
@property (nonatomic, strong) CCMenuItemSprite *clock;
@property (nonatomic, strong) CCMenuItemSprite *spider;
@property (nonatomic, strong) CCMenuItemSprite *lamp;

@property (nonatomic, strong) CCSprite *atticABoy;
@property (nonatomic, strong) CCSprite *atticACat;
@property (nonatomic, strong) CCSprite *atticARat;
@property (nonatomic, strong) CCSprite *atticASword;
@property (nonatomic, strong) CCSprite *atticAHelmet;
@property (nonatomic, strong) CCSprite *atticMirror;

@end

@implementation AtticLayer

int animationCount = 0;

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	AtticLayer *layer = [AtticLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

- (id)init {
    
    if( (self=[super init]) ) {
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"bear.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"clock.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"mouse.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"spider.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"teacup.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"lamp.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticABoy.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticACat.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticARat.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticBCat.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticBACat.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticBAHelmet.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticBARat.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticCABoy.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticCBBoy.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticCCBoy.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticDBoy.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticECat.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticERat.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticFCat.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticFABoy.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticFBBoy.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticFCBoy.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticGCat.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticHCat.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticHABoy.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"atticHBBoy.plist"];
        
        NSArray *sentenceBlock1 = [NSArray arrayWithObjects:
                                   @"Once upon a time",
                                   @"a boy followed his cat",
                                   @"up all the stairs",
                                   @"to where the attic was at.",
                                   nil];
        NSArray *sentenceBlock2 = [NSArray arrayWithObjects:
                                   @"The boy found some armor",
                                   @"made out of lead.",
                                   @"He took the helmet",
                                   @"and put it on his head.",
                                   nil];
        NSArray *sentenceBlock3 = [NSArray arrayWithObjects:
                                   @"Then the boy found",
                                   @"a wooden sword.",
                                   @"So he picked it up",
                                   @"off of the floor.",
                                   nil];
        NSArray *sentenceBlock4 = [NSArray arrayWithObjects:
                                   @"The boy tipped over",
                                   @"and his sword broke a mirror.",
                                   @"And behind all the glass",
                                   @"a magic doorway appeared.",
                                   nil];
        NSArray *sentenceBlock5 = [NSArray arrayWithObjects:
                                   @"The doorway went",
                                   @"to a far away land.",
                                   @"And the boy went inside",
                                   @"with his sword in his hand.",
                                   nil];
        self.sentenceBlocks = [NSMutableArray arrayWithObjects:sentenceBlock1, sentenceBlock2, sentenceBlock3, sentenceBlock4, sentenceBlock5,nil];
        
        [self setUpInitialFrame];
        
    }
     return self;
}

- (void) setUpInitialFrame {
    self.background = [CCSprite spriteWithFile:@"atticBG.png"];
    self.background.anchorPoint = CGPointMake(0, 0);
    
    self.bear = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"bear1_result.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"bear1_result.png"] target:self selector:@selector(bearTapped)];
    [self.bear setPosition:ccp(110, 330)];
    [self.bear setOpacity:0];
    
    self.bearSprite = [CCSprite spriteWithSpriteFrameName:@"bear1_result.png"];
    [self.bearSprite setPosition:ccp(110, 330)];
    
    
    self.clock = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"clock1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"clock1.png"]target:self selector:@selector(clockTapped)];
    [self.clock setPosition:ccp(500, 170)];
    [self.clock setOpacity:0];
    
    self.clockSprite = [CCSprite spriteWithSpriteFrameName:@"clock1.png"];
    [self.clockSprite setPosition:ccp(500, 170)];
    
    self.teacup = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"teacup_1.png"]selectedSprite:[CCSprite spriteWithSpriteFrameName:@"teacup_1.png"]target:self selector:@selector(teacupTapped)];
    [self.teacup setPosition:ccp(325, 450)];
    [self.teacup setOpacity:0];
    
    self.teacupSprite = [CCSprite spriteWithSpriteFrameName:@"teacup_1.png"];
    [self.teacupSprite setPosition:ccp(325, 450)];
    
    self.mouse = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"mouse1.png"]selectedSprite:[CCSprite spriteWithSpriteFrameName:@"mouse1.png"]target:self selector:@selector(mouseTapped)];
    [self.mouse setPosition:ccp(365, 350)];
    [self.mouse setOpacity:0];
    
    self.mouseSprite = [CCSprite spriteWithSpriteFrameName:@"mouse1.png"];
    [self.mouseSprite setPosition:ccp(365, 350)];
    
    self.spider = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"Spider1.png"]selectedSprite:[CCSprite spriteWithSpriteFrameName:@"Spider1.png"]target:self selector:@selector(spiderTapped)];
    [self.spider setPosition:ccp(500, 568)];
    [self.spider setOpacity:0];
    
    self.spiderSprite = [CCSprite spriteWithSpriteFrameName:@"Spider1.png"];
    [self.spiderSprite setPosition:ccp(500, 568)];
    
    self.lamp = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"lamp_1.png"]selectedSprite:[CCSprite spriteWithSpriteFrameName:@"lamp_1.png"]target:self selector:@selector(lampTapped)];
    [self.lamp setPosition:ccp(240, 600)];
    [self.lamp setOpacity:0];
    
    self.lampSprite = [CCSprite spriteWithSpriteFrameName:@"lamp_1.png"];
    [self.lampSprite setPosition:ccp(240, 600)];

    self.clickableMenu = [CCMenu menuWithItems:self.bear, self.clock, self.teacup, self.mouse, self.spider, self.lamp, nil];
    self.clickableMenu.position = CGPointZero;
    
    self.atticABoy = [CCSprite spriteWithSpriteFrameName:@"attic1boy1.png"];
    [self.atticABoy setPosition:ccp(1024/2, 768/2)];
    self.atticACat = [CCSprite spriteWithSpriteFrameName:@"attic1cat1.png"];
    [self.atticACat setPosition:ccp(1024/2, 768/2)];
    self.atticARat = [CCSprite spriteWithSpriteFrameName:@"attic1rat1.png"];
    [self.atticARat setPosition:ccp(1024/2, 768/2)];
    self.atticAHelmet = [CCSprite spriteWithFile:@"attic1helmet.png"];
    [self.atticAHelmet setPosition:ccp(1024/2, 768/2)];
    self.atticASword = [CCSprite spriteWithFile:@"attic1sword.png"];
    [self.atticASword setPosition:ccp(1024/2, 768/2)];
    self.atticMirror = [CCSprite spriteWithFile:@"mirror.png"];
    [self.atticMirror setPosition:ccp(1024/2, 768/2)];
    [self.atticMirror setOpacity:0];

    [self addChild:self.clickableMenu z:1];
    [self addChild:self.bearSprite z:1];
    [self addChild:self.clockSprite z:5];
    [self addChild:self.mouseSprite z:2];
    [self addChild:self.teacupSprite z:1];
    [self addChild:self.spiderSprite z:1];
    [self addChild:self.lampSprite z: 1];
    [self addChild:self.atticABoy z:4];
    [self addChild:self.atticACat z:3];
    [self addChild:self.atticARat z:3];
    [self addChild:self.atticAHelmet z:3];
    [self addChild:self.atticASword z:3];
    [self addChild:self.atticMirror z:3];

    [super setupInitialFrame];
    
    
//    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
//        ([UIScreen mainScreen].scale == 2.0)) {
//        
//        NSArray *spriteArray = [NSArray arrayWithObjects: self.background, self.bear, self.bearSprite, self.clock, self.clockSprite, self.mouse, self.mouseSprite, self.teacup, self.teacupSprite, self.spider, self.spiderSprite, nil];
//        [self doubleSize:spriteArray];
//    }

}

- (void) runAnimation: (int) animationNumber {
    
    if (animationNumber == 1) {
        
        NSMutableArray *boyframes = [NSMutableArray array];
        NSMutableArray *catframes = [NSMutableArray array];
        NSMutableArray *ratframes = [NSMutableArray array];
        
        for (int i = 2; i <= 317; i++) {
            [boyframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic1boy%d.png", i]]];
            [catframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic1cat%d.png", i]]];
            [ratframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic1rat%d.png", i]]];
        }
        
        CCAnimation *boyanimation = [CCAnimation animationWithSpriteFrames:boyframes delay:0.015f];
        CCAnimation *catanimation = [CCAnimation animationWithSpriteFrames:catframes delay:0.015f];
        CCAnimation *ratanimation = [CCAnimation animationWithSpriteFrames:ratframes delay:0.015f];
        
        [self.atticABoy runAction:[CCSequence actions:[CCAnimate actionWithAnimation:boyanimation], nil]];
        [self.atticACat runAction:[CCSequence actions:[CCAnimate actionWithAnimation:catanimation], nil]];
        [self.atticARat runAction:[CCSequence actions:[CCAnimate actionWithAnimation:ratanimation], nil]];
        
    }
    else if (animationNumber == 2) {
        NSMutableArray *catframes = [NSMutableArray array];
        for (int i = 2; i <= 250; i++) {
           
            [catframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic2cat%d.png", i]]];

        }
        CCAnimation *catanimation = [CCAnimation animationWithSpriteFrames:catframes delay:0.025f];
        [self.atticACat runAction:[CCSequence actions:[CCAnimate actionWithAnimation:catanimation], nil]];
    }
    else if (animationNumber == 3) {
        NSMutableArray *catframes = [NSMutableArray array];
        NSMutableArray *helmetframes = [NSMutableArray array];
        NSMutableArray *ratframes = [NSMutableArray array];
        for (int i = 2; i <= 170; i++) {
            [catframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic2Acat%d.png", i]]];
            [helmetframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic2Ahelmet%d.png", i]]];
            [ratframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic2Arat%d.png", i]]];
        }
        CCAnimation *catanimation = [CCAnimation animationWithSpriteFrames:catframes delay:0.025f];
        CCAnimation *helmetanimation = [CCAnimation animationWithSpriteFrames:helmetframes delay:0.025f];
        CCAnimation *ratanimation = [CCAnimation animationWithSpriteFrames:ratframes delay:0.025f];
        [self.atticACat runAction:[CCSequence actions:[CCAnimate actionWithAnimation:catanimation], nil]];
        [self.atticAHelmet runAction:[CCSequence actions:[CCAnimate actionWithAnimation:helmetanimation], nil]];
        [self.atticARat runAction:[CCSequence actions:[CCAnimate actionWithAnimation:ratanimation], nil]];
    }
    else if (animationNumber == 4) {
        [self.atticAHelmet setOpacity:0];
        NSMutableArray *boyframes = [NSMutableArray array];
        for (int i = 2; i <= 166; i++) {
            [boyframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic3boyandhelmet%d.png", i]]];
        }
        CCAnimation *boyanimation = [CCAnimation animationWithSpriteFrames:boyframes delay:0.015f];
        [self.atticABoy runAction:[CCSequence actions:[CCAnimate actionWithAnimation:boyanimation], nil]];
    }
    else if (animationNumber == 5) {
        [self.atticASword setOpacity:0];
        NSMutableArray *boyframes = [NSMutableArray array];
        for (int i = 2; i <= 88; i++) {
            [boyframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic4boyandsword%d.png", i]]];
        }
        CCAnimation *boyanimation = [CCAnimation animationWithSpriteFrames:boyframes delay:0.015f];
        [self.atticABoy runAction:[CCSequence actions:[CCAnimate actionWithAnimation:boyanimation], nil]];
    }
    else if (animationNumber == 6) {
        NSMutableArray *catframes = [NSMutableArray array];
        NSMutableArray *ratframes = [NSMutableArray array];
        for (int i = 2; i <= 220; i++) {
            [catframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic5cat%d.png", i]]];
            [ratframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic5rat%d.png", i]]];
        }
        CCAnimation *catanimation = [CCAnimation animationWithSpriteFrames:catframes delay:0.025f];
        CCAnimation *ratanimation = [CCAnimation animationWithSpriteFrames:ratframes delay:0.025f];
        [self.atticACat runAction:[CCSequence actions:[CCAnimate actionWithAnimation:catanimation], nil]];
        [self.atticARat runAction:[CCSequence actions:[CCAnimate actionWithAnimation:ratanimation], nil]];
    }
    else if (animationNumber == 7) {
        NSMutableArray *catframes = [NSMutableArray array];
        NSMutableArray *boyframes = [NSMutableArray array];
        for (int i = 2; i <= 227; i++) {
            [catframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic6cat%d.png", i]]];
            [boyframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic6boyswordandmirror%d.png", i]]];
        }
        CCAnimation *catanimation = [CCAnimation animationWithSpriteFrames:catframes delay:0.025f];
        CCAnimation *boyanimation = [CCAnimation animationWithSpriteFrames:boyframes delay:0.025f];
        [self.atticACat runAction:[CCSequence actions:[CCAnimate actionWithAnimation:catanimation], nil]];
        [self.atticABoy runAction:[CCSequence actions:[CCAnimate actionWithAnimation:boyanimation], nil]];
    }
    else if (animationNumber == 8) {
        NSMutableArray *catframes = [NSMutableArray array];
        for (int i = 2; i <= 119; i++) {
            [catframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic7cat%d.png", i]]];
        }
        CCAnimation *catanimation = [CCAnimation animationWithSpriteFrames:catframes delay:0.025f];
        [self.atticACat runAction:[CCSequence actions:[CCAnimate actionWithAnimation:catanimation], nil]];
    }
    else if (animationNumber == 9) {
        [self.atticMirror setOpacity:255];
        NSMutableArray *catframes = [NSMutableArray array];
        NSMutableArray *boyframes = [NSMutableArray array];
        for (int i = 2; i <= 274; i++) {
            [catframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic8cat%d.png", i]]];
            [boyframes addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"attic8boyandsword%d.png", i]]];
        }
        CCAnimation *catanimation = [CCAnimation animationWithSpriteFrames:catframes delay:0.025f];
        CCAnimation *boyanimation = [CCAnimation animationWithSpriteFrames:boyframes delay:0.025f];
        [self.atticACat runAction:[CCSequence actions:[CCAnimate actionWithAnimation:catanimation], nil]];
        [self.atticABoy runAction:[CCSequence actions:[CCAnimate actionWithAnimation:boyanimation], nil]];
    }
}

- (void) nextSceneTapped {
    NSLog(@"nextSceneTapped");
    
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[FarawaylandLayer scene] withColor:ccWHITE]];
    
}

- (void) bearTapped {
    NSLog(@"bearTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 42; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"bear%d_result.png", i]]];
    }
    
    CCAnimation *bearAnimation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.bearSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:bearAnimation], nil]];
}

- (void) teacupTapped {
    NSLog(@"teacupTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 38; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"teacup_%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.teacupSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

- (void) mouseTapped {
    NSLog(@"mouseTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 67; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"mouse%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.mouseSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

- (void) clockTapped {
    NSLog(@"clockTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 46; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"clock%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.clockSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

- (void) spiderTapped {
    NSLog(@"spiderTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 37; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"Spider%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.spiderSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

- (void) lampTapped {
    NSLog(@"lampTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 45; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"lamp_%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.lampSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}
@end
