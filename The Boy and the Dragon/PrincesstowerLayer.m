//
//  PrincesstowerLayer.m
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 5/31/14.
//  Copyright 2014 Muvu Media. All rights reserved.
//

#import "PrincesstowerLayer.h"
#import "AtticreturnLayer.h"

@interface PrincesstowerLayer ()

@property (nonatomic, strong) CCSprite *henSprite;
@property (nonatomic, strong) CCMenuItemSprite *hen;

@end

@implementation PrincesstowerLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	PrincesstowerLayer *layer = [PrincesstowerLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

- (id)init {
    if( (self=[super init]) ) {
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hen.plist"];
        
        NSArray *sentenceBlock1 = [NSArray arrayWithObjects:
                                   @"The boy flew up the tower",
                                   @"from the ground below",
                                   @"to rescue the princess",
                                   @"who was standing by the window.",
                                   nil];
        
        self.sentenceBlocks = [NSMutableArray arrayWithObjects:sentenceBlock1, nil];
        
        [self setUpInitialFrame];
        
    }
    return self;
}

- (void) setUpInitialFrame {
    self.background = [CCSprite spriteWithFile:@"princessBG.png"];
    self.background.anchorPoint = CGPointMake(0, 0);
    
    self.hen = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"hen1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"hen1.png"] target:self selector:@selector(henTapped)];
    [self.hen setPosition:ccp(900, 100)];
    [self.hen setOpacity:0];
    
    self.henSprite = [CCSprite spriteWithSpriteFrameName:@"hen1.png"];
    [self.henSprite setPosition:ccp(830, 160)];
    
    self.clickableMenu = [CCMenu menuWithItems:self.hen, nil];
    self.clickableMenu.position = CGPointZero;
    
    [self addChild:self.clickableMenu z:1];
    [self addChild:self.henSprite z:1];
    
    [super setupInitialFrame];
}

- (void) nextSceneTapped {
    [super nextSceneTapped];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[AtticreturnLayer scene] withColor:ccWHITE]];
    
}

-(void) henTapped {
    NSLog(@"henTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 38; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"hen%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.henSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

@end
