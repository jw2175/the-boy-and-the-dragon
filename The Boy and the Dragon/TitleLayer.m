//
//  TitleLayer.m
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 6/1/14.
//  Copyright 2014 Muvu Media. All rights reserved.
//

#import "TitleLayer.h"
#import "AtticLayer.h"


@implementation TitleLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	TitleLayer *layer = [TitleLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

- (id)init {
    if( (self=[super init]) ) {
        
        [self setUpInitialFrame];
        
    }
    return self;
}

- (void) setUpInitialFrame {
    self.background = [CCSprite spriteWithFile:@"titleBG.png"];
    self.background.anchorPoint = CGPointMake(0, 0);
    
    self.title = [CCSprite spriteWithFile:@"title.png"];
    self.title.anchorPoint = CGPointMake(0, 0);
    
    [super setupInitialFrame];
    
    [self addChild:self.title z:1];
    
    [self.nextAnimationButton setOpacity:0];
    [self.nextSentenceButton setOpacity:0];
    [self.nextAnimationButton setIsEnabled:NO];
    [self.nextSentenceButton setIsEnabled:NO];
    [self.pauseButton setOpacity:0];
    [self.pauseButton setIsEnabled:NO];
}

- (void) nextSceneTapped {
    [super nextSceneTapped];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[AtticLayer scene] withColor:ccWHITE]];
    
}

@end
