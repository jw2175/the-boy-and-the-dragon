//
//  FarawaylandLayer.m
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 5/31/14.
//  Copyright 2014 Muvu Media. All rights reserved.
//

#import "FarawaylandLayer.h"
#import "KnightsforestLayer.h"

@interface FarawaylandLayer ()

@property (nonatomic, strong) CCSprite *daisiesSprite;
@property (nonatomic, strong) CCSprite *orangeflowersSprite;
@property (nonatomic, strong) CCSprite *cowSprite;
@property (nonatomic, strong) CCSprite *sheepSprite;
@property (nonatomic, strong) CCSprite *dragonSprite;

@property (nonatomic, strong) CCMenuItemSprite *daisies;
@property (nonatomic, strong) CCMenuItemSprite *orangeflowers;
@property (nonatomic, strong) CCMenuItemSprite *cow;
@property (nonatomic, strong) CCMenuItemSprite *sheep;

@property (nonatomic, strong) NSMutableArray *daisiesFrames;
@property (nonatomic, strong) NSMutableArray *orangeflowersFrames;
@property (nonatomic, strong) NSMutableArray *cowFrames;
@property (nonatomic, strong) NSMutableArray *sheepFrames;
@property (nonatomic, strong) NSMutableArray *dragonFrames;

@property (nonatomic, strong) CCAnimation *daisiesAnimation;
@property (nonatomic, strong) CCAnimation *orangeflowersAnimation;
@property (nonatomic, strong) CCAnimation *cowAnimation;
@property (nonatomic, strong) CCAnimation *sheepAnimation;
@property (nonatomic, strong) CCAnimation *dragonAnimation;

@property (nonatomic, strong) CCSprite *falBoy;
@property (nonatomic, strong) CCSprite *falFlower;

@property (nonatomic, strong) NSMutableArray *boyFrames1;
@property (nonatomic, strong) NSMutableArray *boyFrames2;
@property (nonatomic, strong) NSMutableArray *boyFrames3;
@property (nonatomic, strong) NSMutableArray *boyFrames4;
@property (nonatomic, strong) NSMutableArray *flowerFrames1;
@property (nonatomic, strong) NSMutableArray *flowerFrames2;
@property (nonatomic, strong) NSMutableArray *flowerFrames3;
@property (nonatomic, strong) NSMutableArray *flowerFrames4;

@property (nonatomic, strong) CCAnimation *boyAnimation1;
@property (nonatomic, strong) CCAnimation *boyAnimation2;
@property (nonatomic, strong) CCAnimation *boyAnimation3;
@property (nonatomic, strong) CCAnimation *boyAnimation4;
@property (nonatomic, strong) CCAnimation *flowerAnimation1;
@property (nonatomic, strong) CCAnimation *flowerAnimation2;
@property (nonatomic, strong) CCAnimation *flowerAnimation3;
@property (nonatomic, strong) CCAnimation *flowerAnimation4;

@end


@implementation FarawaylandLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	FarawaylandLayer *layer = [FarawaylandLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

- (id)init {
    if( (self=[super init]) ) {
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"cow.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"daisies.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"orangeflowers.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"sheep.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"dragon.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"falA.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"falB.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"falC.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"falDBoy.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"falDFlower.plist"];
        
        
        NSArray *sentenceBlock1 = [NSArray arrayWithObjects:
                                   @"The boy found himself",
                                   @"on a long road to a tower,",
                                   @"over hills and through forests,",
                                   @"where he met a talking flower.",
                                   nil];
        NSArray *sentenceBlock2 = [NSArray arrayWithObjects:
                                   @"And so the boy",
                                   @"walked along the way",
                                   @"down the road",
                                   @"to save the day.",
                                   nil];
        
        self.sentenceBlocks = [NSMutableArray arrayWithObjects:sentenceBlock1, sentenceBlock2, nil];
        
        [self setUpAnimations];
        [self setUpInitialFrame];
        
    }
    return self;
}

-(void)setUpAnimations {
    self.daisiesFrames = [NSMutableArray array];
    self.orangeflowersFrames = [NSMutableArray array];
    self.cowFrames = [NSMutableArray array];
    self.sheepFrames = [NSMutableArray array];
    self.dragonFrames = [NSMutableArray array];
    
    for (int i = 2; i <= 23; i++) {
        [self.daisiesFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"daisiess%d.png", i]]];
    }
    for (int i = 2; i <= 72; i++) {
        [self.orangeflowersFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"OrangeFlowers%d.png", i]]];
    }
    for (int i = 2; i <= 46; i++) {
        [self.cowFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"cow%d.png", i]]];
    }
    for (int i = 2; i <= 64; i++) {
        [self.sheepFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"Sheep%d_result.png", i]]];
    }
    for (int i = 2; i <= 100; i++) {
        [self.dragonFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"dragon%d.png", i]]];
    }
    
    self.daisiesAnimation = [CCAnimation animationWithSpriteFrames:self.daisiesFrames delay:0.05f];
    self.orangeflowersAnimation = [CCAnimation animationWithSpriteFrames:self.orangeflowersFrames delay:0.05f];
    self.cowAnimation = [CCAnimation animationWithSpriteFrames:self.cowFrames delay:0.05f];
    self.sheepAnimation = [CCAnimation animationWithSpriteFrames:self.sheepFrames delay:0.05f];
    self.dragonAnimation = [CCAnimation animationWithSpriteFrames:self.dragonFrames delay:0.05f];
    
    self.boyFrames1 = [NSMutableArray array];
    self.boyFrames2 = [NSMutableArray array];
    self.boyFrames3 = [NSMutableArray array];
    self.boyFrames4 = [NSMutableArray array];
    self.flowerFrames1 = [NSMutableArray array];
    self.flowerFrames2 = [NSMutableArray array];
    self.flowerFrames3 = [NSMutableArray array];
    self.flowerFrames4 = [NSMutableArray array];
    
    for (int i = 2; i <= 133; i++) {
        [self.boyFrames1 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"farawayland1boy%d.png", i]]];
        [self.flowerFrames1 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"farawayland1flower%d.png", i]]];
    }
    for (int i = 2; i <= 240; i++) {
        [self.boyFrames2 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"farawayland2boy%d.png", i]]];
        [self.flowerFrames2 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"farawayland2flower%d.png", i]]];
    }
    for (int i = 2; i <= 135; i++) {
        [self.boyFrames3 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"farAwayLand3Boy%d.png", i]]];
        [self.flowerFrames3 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"farawayland3flower%d.png", i]]];
    }
    for (int i = 2; i <= 436; i++) {
        [self.boyFrames4 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"farawayland4boy%d.png", i]]];
        [self.flowerFrames4 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"farawayland4flower%d.png", i]]];
    }
    
    self.boyAnimation1 = [CCAnimation animationWithSpriteFrames:self.boyFrames1 delay:0.025f];
    self.boyAnimation2 = [CCAnimation animationWithSpriteFrames:self.boyFrames2 delay:0.025f];
    self.boyAnimation3 = [CCAnimation animationWithSpriteFrames:self.boyFrames3 delay:0.025f];
    self.boyAnimation4 = [CCAnimation animationWithSpriteFrames:self.boyFrames4 delay:0.025f];
    self.flowerAnimation1 = [CCAnimation animationWithSpriteFrames:self.flowerFrames1 delay:0.025f];
    self.flowerAnimation2 = [CCAnimation animationWithSpriteFrames:self.flowerFrames2 delay:0.025f];
    self.flowerAnimation3 = [CCAnimation animationWithSpriteFrames:self.flowerFrames3 delay:0.025f];
    self.flowerAnimation4 = [CCAnimation animationWithSpriteFrames:self.flowerFrames4 delay:0.025f];
}

- (void) setUpInitialFrame {
    self.background = [CCSprite spriteWithFile:@"farawaylandBG.png"];
    self.background.anchorPoint = CGPointMake(0, 0);
    
    self.sheep = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"Sheep1_result.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"Sheep1_result.png"] target:self selector:@selector(sheepTapped)];
    [self.sheep setPosition:ccp(170, 280)];
    [self.sheep setOpacity:0];
    
    self.sheepSprite = [CCSprite spriteWithSpriteFrameName:@"Sheep1_result.png"];
    [self.sheepSprite setPosition:ccp(170, 280)];
    
    self.cow = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"cow1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"cow1.png"] target:self selector:@selector(cowTapped)];
    [self.cow setPosition:ccp(840, 260)];
    [self.cow setScale:0.8];
    [self.cow setOpacity:0];
    
    self.cowSprite = [CCSprite spriteWithSpriteFrameName:@"cow1.png"];
    [self.cowSprite setPosition:ccp(840, 260)];
    [self.cowSprite setScale:0.8];
    
    self.daisies = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"daisiess1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"daisiess1.png"] target:self selector:@selector(daisiesTapped)];
    [self.daisies setPosition:ccp(110, 130)];
    [self.daisies setOpacity:0];
    
    self.daisiesSprite = [CCSprite spriteWithSpriteFrameName:@"daisiess1.png"];
    [self.daisiesSprite setPosition:ccp(110, 140)];
    
    self.orangeflowers = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithFile:@"orangeflowersclick.png"] selectedSprite:[CCSprite spriteWithFile:@"orangeflowersclick.png"] target:self selector:@selector(orangeflowersTapped)];
    [self.orangeflowers setPosition:ccp(805, 155)];
    [self.orangeflowers setOpacity:0];
    
    self.orangeflowersSprite = [CCSprite spriteWithSpriteFrameName:@"OrangeFlowers1.png"];
    [self.orangeflowersSprite setPosition:ccp(520, 260)];
    
    self.dragonSprite = [CCSprite spriteWithSpriteFrameName:@"dragon1.png"];
    [self.dragonSprite setPosition:ccp(813, 471)];
    
    self.clickableMenu = [CCMenu menuWithItems:self.cow, self.daisies, self.orangeflowers, self.sheep, nil];
    self.clickableMenu.position = CGPointZero;
    
    self.falBoy = [CCSprite spriteWithSpriteFrameName:@"farawayland1boy1.png"];
    [self.falBoy setPosition:ccp(1024/2, 768/2)];
    self.falFlower = [CCSprite spriteWithSpriteFrameName:@"farawayland1flower1.png"];
    [self.falFlower setPosition:ccp(1024/2, 768/2)];
    
    [self addChild:self.clickableMenu z:1];
    [self addChild:self.cowSprite z:1];
    [self addChild:self.daisiesSprite z:1];
    [self addChild:self.orangeflowersSprite z:2];
    [self addChild:self.sheepSprite z:1];
    [self addChild:self.dragonSprite z:1];
    [self addChild:self.falBoy z:2];
    [self addChild:self.falFlower z:2];
    
    [super setupInitialFrame];
    
    [self animateDragon];
}

- (void) nextSceneTapped {
    [super nextSceneTapped];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[KnightsforestLayer scene] withColor:ccWHITE]];
}

- (void) animateDragon {
    NSLog(@"animating dragon");
    [self.dragonSprite runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:self.dragonAnimation]]];
}

- (void) sheepTapped {
    NSLog(@"sheepTapped");
    [self.sheepSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.sheepAnimation], nil]];
}

- (void) cowTapped {
    NSLog(@"cowTapped");
    [self.cowSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.cowAnimation], nil]];
}

-(void) daisiesTapped {
    NSLog(@"daisiesTapped");
    [self.daisiesSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.daisiesAnimation], nil]];
}

-(void) orangeflowersTapped {
    NSLog(@"orangeflowersTapped");
    [self.orangeflowersSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.orangeflowersAnimation], nil]];
}

- (void) runAnimation: (int) animationNumber {
    
    if (animationNumber == 1) {
        [self.falBoy runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.boyAnimation1], nil]];
        [self.falFlower runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.flowerAnimation1], nil]];
    }
    else if (animationNumber == 2) {
        [self.falBoy runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.boyAnimation2], nil]];
        [self.falFlower runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.flowerAnimation2], nil]];
    }
    else if (animationNumber == 3) {
        [self.falBoy runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.boyAnimation3], nil]];
        [self.falFlower runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.flowerAnimation3], nil]];
    }
    else if (animationNumber == 4) {
        [self.falBoy runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.boyAnimation4], nil]];
        [self.falFlower runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.flowerAnimation4], nil]];
    }
}
@end
