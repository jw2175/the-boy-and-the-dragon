//
//  VoiceRecEngine.h
//  The Boy and The Bears
//
//  Created by P Balasubramanian on 4/26/13.
//
//

#import <Foundation/Foundation.h>
#import <OpenEars/PocketsphinxController.h>
#import <OpenEars/OpenEarsEventsObserver.h>
#import <OpenEars/LanguageModelGenerator.h>

@interface VoiceRecEngine : NSObject {
    PocketsphinxController *pocketsphinxController;
    OpenEarsEventsObserver *openEarsEventsObserver;
    LanguageModelGenerator *lmGenerator;
    bool voiceRecStarted;
    bool voiceRecSuspended;
    bool voiceRecOn;
}
@property (strong, nonatomic) LanguageModelGenerator *lmGenerator;
@property (strong, nonatomic) PocketsphinxController *pocketsphinxController;
@property (strong, nonatomic) OpenEarsEventsObserver *openEarsEventsObserver;
@property (nonatomic, getter=isVoiceRecOn) bool voiceRecOn;
@property (nonatomic, getter=isVoiceRecStarted) bool voiceRecStarted;
@property (nonatomic, getter=isVoiceRecSuspended) bool voiceRecSuspended;


+ (id)sharedInstance;

@end
