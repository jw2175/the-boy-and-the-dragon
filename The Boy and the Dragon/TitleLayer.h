//
//  TitleLayer.h
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 6/1/14.
//  Copyright 2014 Muvu Media. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "MuvuSceneLayer.h"

@interface TitleLayer : MuvuSceneLayer {
    
}

@property (nonatomic, strong) CCSprite *title;

@end
