//
//  WizardshouseLayer.m
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 5/31/14.
//  Copyright 2014 Muvu Media. All rights reserved.
//

#import "WizardshouseLayer.h"
#import "DragonsgateLayer.h"

@interface WizardshouseLayer ()

@property (nonatomic, strong) CCSprite *broomSprite;
@property (nonatomic, strong) CCSprite *bucketSprite;
@property (nonatomic, strong) CCSprite *crowSprite;
@property (nonatomic, strong) CCSprite *potionSprite;
@property (nonatomic, strong) CCSprite *smokeSprite;

@property (nonatomic, strong) CCMenuItemSprite *broom;
@property (nonatomic, strong) CCMenuItemSprite *bucket;
@property (nonatomic, strong) CCMenuItemSprite *crow;
@property (nonatomic, strong) CCMenuItemSprite *potion;

@end

@implementation WizardshouseLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	WizardshouseLayer *layer = [WizardshouseLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

- (id)init {
    if( (self=[super init]) ) {
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"broom.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"bucket.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"crow.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"potion.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"smoke.plist"];
        
        NSArray *sentenceBlock1 = [NSArray arrayWithObjects:
                                   @"Then the boy ran down the road",
                                   @"when out of thin air,",
                                   @"a wizard appeared",
                                   @"who had long gray hair.",
                                   nil];
        NSArray *sentenceBlock2 = [NSArray arrayWithObjects:
                                   @"The boy stood",
                                   @"and watched in wonder",
                                   @"as the magic wand",
                                   @"lit up with thunder.",
                                   nil];
        NSArray *sentenceBlock3 = [NSArray arrayWithObjects:
                                   @"When the wizard cast his spell",
                                   @"the boy flipped down his mask",
                                   @"and it bounced the spell",
                                   @"back where it was cast.",
                                   nil];
        
        self.sentenceBlocks = [NSMutableArray arrayWithObjects:sentenceBlock1, sentenceBlock2, sentenceBlock3, nil];
        
        [self setUpInitialFrame];
        
    }
    return self;
}

- (void) setUpInitialFrame {
    self.background = [CCSprite spriteWithFile:@"wizardBG.png"];
    self.background.anchorPoint = CGPointMake(0, 0);
    
    self.potion = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"potion1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"potion1.png"] target:self selector:@selector(potionTapped)];
    [self.potion setPosition:ccp(570, 310)];
    [self.potion setOpacity:0];
    
    self.potionSprite = [CCSprite spriteWithSpriteFrameName:@"potion1.png"];
    [self.potionSprite setPosition:ccp(570, 310)];
    
    self.broom = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"broom1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"broom1.png"] target:self selector:@selector(broomTapped)];
    [self.broom setPosition:ccp(110, 270)];
    [self.broom setOpacity:0];
    
    self.broomSprite = [CCSprite spriteWithSpriteFrameName:@"broom1.png"];
    [self.broomSprite setPosition:ccp(110, 270)];
    
    self.bucket = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"bucket1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"bucket1.png"] target:self selector:@selector(bucketTapped)];
    [self.bucket setPosition:ccp(403, 309)];
    [self.bucket setOpacity:0];
    
    self.bucketSprite = [CCSprite spriteWithSpriteFrameName:@"bucket1.png"];
    [self.bucketSprite setPosition:ccp(403, 309)];
    
    self.crow = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"crow1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"crow1.png"] target:self selector:@selector(crowTapped)];
    [self.crow setPosition:ccp(870, 270)];
    [self.crow setOpacity:0];
    
    self.crowSprite = [CCSprite spriteWithSpriteFrameName:@"crow1.png"];
    [self.crowSprite setPosition:ccp(870, 240)];
    
    self.smokeSprite = [CCSprite spriteWithSpriteFrameName:@"smoke1.png"];
    [self.smokeSprite setPosition:ccp(630, 573)];
    
    self.clickableMenu = [CCMenu menuWithItems:self.broom, self.bucket, self.crow, self.potion, nil];
    self.clickableMenu.position = CGPointZero;
    
    [self addChild:self.clickableMenu z:1];
    [self addChild:self.broomSprite z:1];
    [self addChild:self.bucketSprite z:1];
    [self addChild:self.crowSprite z:1];
    [self addChild:self.potionSprite z:1];
    [self addChild:self.smokeSprite z:1];
    
    [super setupInitialFrame];
    
    [self animateSmoke];
}

- (void) nextSceneTapped {
    [super nextSceneTapped];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[DragonsgateLayer scene] withColor:ccWHITE]];
    
}

-(void) animateSmoke {
    NSLog(@"animating smoke");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 33; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"smoke%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.smokeSprite runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:animation]]];
}

-(void) broomTapped {
    NSLog(@"broomTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 81; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"broom%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.broomSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

-(void) potionTapped {
    NSLog(@"potionTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 33; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"potion%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.potionSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

-(void) bucketTapped {
    NSLog(@"bucketTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 26; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"bucket%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.bucketSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

-(void) crowTapped {
    NSLog(@"crowTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 56; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"crow%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.crowSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

@end
