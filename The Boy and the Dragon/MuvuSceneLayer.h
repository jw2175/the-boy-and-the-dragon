//
//  MuvuSceneLayer.h
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 5/31/14.
//  Copyright 2014 Muvu Media. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface MuvuSceneLayer : CCLayer {

}

//temporary navigation buttons
@property (nonatomic, strong) CCMenuItemLabel *nextSceneButton;
@property (nonatomic, strong) CCMenuItemLabel *nextSentenceButton;
@property (nonatomic, strong) CCMenuItemLabel *nextAnimationButton;

@property (nonatomic) int animationCount;
@property (nonatomic) int sentenceCount;

@property (nonatomic, strong) CCMenuItemImage *pauseButton;
@property (nonatomic, strong) CCSprite *pauseBackground;
@property (nonatomic, strong) CCMenuItemImage *resumeButton;
@property (nonatomic, strong) CCMenuItemImage *startoverButton;

@property (nonatomic, strong) CCMenu *mainMenu;
@property (nonatomic, strong) CCMenu *clickableMenu;
@property (nonatomic, strong) CCSprite *background;

@property (nonatomic, strong) CCLabelTTF *sent1;
@property (nonatomic, strong) CCLabelTTF *sent2;
@property (nonatomic, strong) CCLabelTTF *sent3;
@property (nonatomic, strong) CCLabelTTF *sent4;

@property (nonatomic, strong) CCMenu *sentenceMenu1;
@property (nonatomic, strong) CCMenu *sentenceMenu2;
@property (nonatomic, strong) CCMenu *sentenceMenu3;
@property (nonatomic, strong) CCMenu *sentenceMenu4;

@property (nonatomic, strong) NSArray *sentenceBlocks;

+(CCScene *) scene;
-(void) setupInitialFrame;
-(void) nextSceneTapped;
-(void) runAnimation: (int) animationNumber;
-(void) displaySentenceBlock: (int) sentenceNumber;

-(void) doubleSize: (NSArray *) spriteArray;

@end
