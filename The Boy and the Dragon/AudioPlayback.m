//
//  AudioPlayback.m
//  The Boy and The Bears
//
//  Created by P Balasubramanian on 4/24/13.
//
//

#import "AudioPlayback.h"
#import "VoiceRecEngine.h"

@implementation AudioPlayback

static int suspendCount;

-(id)init
{
    self = [super init];
    suspendCount = 0;
    return self;
}

-(void)PlaySoundFile:(NSString *) sound_file_name
{
    
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:sound_file_name ofType:@".mp3"];
    
    NSError *error = nil;
    AVAudioPlayer *player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:soundPath] error:&error];
    
    player.volume = 3.0;
    player.numberOfLoops = 0;
    player.delegate = self;
    [player prepareToPlay];
    
    suspendCount++;

    VoiceRecEngine* sharedVoiceRecEngine = [VoiceRecEngine sharedInstance];
    if ([sharedVoiceRecEngine isVoiceRecOn])
    {

        if (sharedVoiceRecEngine.voiceRecStarted && !sharedVoiceRecEngine.voiceRecSuspended)
        {
            [sharedVoiceRecEngine.pocketsphinxController suspendRecognition];
        }
    }
    
    [player play];
}

-(void)PlaySoundFile:(NSString *) sound_file_name AtTime: (NSTimeInterval) interval
{
    
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:sound_file_name ofType:@".mp3"];
    
    NSError *error = nil;
    AVAudioPlayer *player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:soundPath] error:&error];
    
    NSTimeInterval time = player.deviceCurrentTime + interval;
    player.volume = 3.0;
    player.numberOfLoops = 0;
    player.delegate = self;
    [player prepareToPlay];
    
    suspendCount++;

    VoiceRecEngine* sharedVoiceRecEngine = [VoiceRecEngine sharedInstance];
    if ([sharedVoiceRecEngine isVoiceRecOn])
    {

        if (sharedVoiceRecEngine.voiceRecStarted && !sharedVoiceRecEngine.voiceRecSuspended && !suspendCount)
        {
            [sharedVoiceRecEngine.pocketsphinxController suspendRecognition];
        }
    }
    
    [player playAtTime:time];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    
    [player release];
    player=nil;
    
    NSLog(@"audioPlayerDidFinishPlaying");
            
    suspendCount--;

    VoiceRecEngine* sharedVoiceRecEngine = [VoiceRecEngine sharedInstance];
    if ([sharedVoiceRecEngine isVoiceRecOn])
    {
        
        if (sharedVoiceRecEngine.voiceRecStarted && sharedVoiceRecEngine.voiceRecSuspended && !suspendCount)
            [sharedVoiceRecEngine.pocketsphinxController resumeRecognition];
    }
}

@end
