//
//  GiantshillLayer.m
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 5/31/14.
//  Copyright 2014 Muvu Media. All rights reserved.
//

#import "GiantshillLayer.h"
#import "WizardshouseLayer.h"

@interface GiantshillLayer ()

@property (nonatomic, strong) CCSprite *bluebirdSprite;
@property (nonatomic, strong) CCSprite *gopherSprite;
@property (nonatomic, strong) CCSprite *peacockSprite;
@property (nonatomic, strong) CCSprite *lizardSprite;

@property (nonatomic, strong) CCMenuItemSprite *bluebird;
@property (nonatomic, strong) CCMenuItemSprite *gopher;
@property (nonatomic, strong) CCMenuItemSprite *peacock;
@property (nonatomic, strong) CCMenuItemSprite *lizard;

@property (nonatomic, strong) CCSprite *ghBoy;
@property (nonatomic, strong) CCSprite *ghGiant;
@property (nonatomic, strong) CCSprite *ghWind;

@property (nonatomic, strong) NSMutableArray *boyFrames1;
@property (nonatomic, strong) NSMutableArray *boyFrames3;
@property (nonatomic, strong) NSMutableArray *boyFrames5;
@property (nonatomic, strong) NSMutableArray *giantFrames2;
@property (nonatomic, strong) NSMutableArray *giantFrames3;
@property (nonatomic, strong) NSMutableArray *giantFrames4;
@property (nonatomic, strong) NSMutableArray *giantFrames5;
@property (nonatomic, strong) NSMutableArray *windFrames5;

@property (nonatomic, strong) CCAnimation *boyAnimation1;
@property (nonatomic, strong) CCAnimation *boyAnimation3;
@property (nonatomic, strong) CCAnimation *boyAnimation5;
@property (nonatomic, strong) CCAnimation *giantAnimation2;
@property (nonatomic, strong) CCAnimation *giantAnimation3;
@property (nonatomic, strong) CCAnimation *giantAnimation4;
@property (nonatomic, strong) CCAnimation *giantAnimation5;
@property (nonatomic, strong) CCAnimation *windAnimation5;

@end

int lizardcolor;

@implementation GiantshillLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	GiantshillLayer *layer = [GiantshillLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

- (id)init {
    if( (self=[super init]) ) {
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"bluebird.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"gopher.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"peacock.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"lizard.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"ghABoy.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"ghAGiant.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"ghCBoy.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"ghCGiant.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"ghDAGiant.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"ghDBGiant.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"ghEBoyGiant.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"ghEWind.plist"];
        
        
        
        NSArray *sentenceBlock1 = [NSArray arrayWithObjects:
                                   @"The boy ran up",
                                   @"a hill covered in grass",
                                   @"where there stood a giant",
                                   @"who would not let him pass.",
                                   nil];
        NSArray *sentenceBlock2 = [NSArray arrayWithObjects:
                                   @"So the boy slowed down",
                                   @"from a run to a walk.",
                                   @"But the giant got mad",
                                   @"and picked up a rock.",
                                   nil];
        NSArray *sentenceBlock3 = [NSArray arrayWithObjects:
                                   @"But then there came",
                                   @"a big gust of wind",
                                   @"and it blew the rock",
                                   @"down on the giant's chin.",
                                   nil];
        NSArray *sentenceBlock4 = [NSArray arrayWithObjects:
                                   @"So the boy passed the giant",
                                   @"with a great big leap,",
                                   @"then he kept on running",
                                   @"as the giant fell asleep.",
                                   nil];
        
        self.sentenceBlocks = [NSMutableArray arrayWithObjects:sentenceBlock1, sentenceBlock2, sentenceBlock3, sentenceBlock4, nil];
        
        [self setUpAnimations];
        [self setUpInitialFrame];
        
    }
    return self;
}

-(void)setUpAnimations {
    self.boyFrames1 = [NSMutableArray array];
    self.boyFrames3 = [NSMutableArray array];
    self.boyFrames5 = [NSMutableArray array];
    self.giantFrames2 = [NSMutableArray array];
    self.giantFrames3 = [NSMutableArray array];
    self.giantFrames4 = [NSMutableArray array];
    self.giantFrames5 = [NSMutableArray array];
    self.windFrames5 = [NSMutableArray array];
    
    
    for (int i = 2; i <= 291; i++) {
        [self.boyFrames1 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"GiantsHill1Boy%d.png", i]]];
    }
    for (int i = 2; i <= 241; i++) {
        [self.giantFrames2 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"GiantsHill2Giant%d.png", i]]];
    }
    for (int i = 2; i <= 129; i++) {
        [self.boyFrames3 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"giantshill3boy%d.png", i]]];
        [self.giantFrames3 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"giantshill3giant%d.png", i]]];
    }
    for (int i = 2; i <= 181; i++) {
        [self.giantFrames4 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"GiantsHill4Giant%d.png", i]]];
    }
    for (int i = 2; i <= 109; i++) {
        [self.boyFrames5 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"GiantsHill5Boy%d.png", i]]];
        [self.giantFrames5 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"GiantsHill5Giant%d.png", i]]];
        [self.windFrames5 addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"GiantsHill5Wind%d.png", i]]];
    }
    
    
    self.boyAnimation1 = [CCAnimation animationWithSpriteFrames:self.boyFrames1 delay:0.025f];
    self.giantAnimation2 = [CCAnimation animationWithSpriteFrames:self.giantFrames2 delay:0.025f];
    self.boyAnimation3 = [CCAnimation animationWithSpriteFrames:self.boyFrames3 delay:0.025f];
    self.giantAnimation3 = [CCAnimation animationWithSpriteFrames:self.giantFrames3 delay:0.025f];
    self.giantAnimation4 = [CCAnimation animationWithSpriteFrames:self.giantFrames4 delay:0.025f];
    self.boyAnimation5 = [CCAnimation animationWithSpriteFrames:self.boyFrames5 delay:0.025f];
    self.giantAnimation5 = [CCAnimation animationWithSpriteFrames:self.giantFrames5 delay:0.025f];
    self.windAnimation5 = [CCAnimation animationWithSpriteFrames:self.windFrames5 delay:0.025f];
}

- (void) setUpInitialFrame {
    self.background = [CCSprite spriteWithFile:@"giantshillBG.png"];
    self.background.anchorPoint = CGPointMake(0, 0);
    
    self.lizard = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"lizardgreen1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"lizardgreen1.png"] target:self selector:@selector(lizardTapped)];
    [self.lizard setPosition:ccp(650, 60)];
    [self.lizard setOpacity:0];
    lizardcolor = 0;
    
    self.lizardSprite = [CCSprite spriteWithSpriteFrameName:@"lizardgreen1.png"];
    [self.lizardSprite setPosition:ccp(650, 60)];
    
    self.bluebird = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"bluebird1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"bluebird1.png"] target:self selector:@selector(bluebirdTapped)];
    [self.bluebird setPosition:ccp(70, 500)];
    [self.bluebird setOpacity:0];
    [self.bluebird setScale:1.25];
    
    self.bluebirdSprite = [CCSprite spriteWithSpriteFrameName:@"bluebird1.png"];
    [self.bluebirdSprite setPosition:ccp(70, 500)];
    [self.bluebirdSprite setScale:1.25];
    
    self.gopher = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"gopher1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"gopher1.png"] target:self selector:@selector(gopherTapped)];
    [self.gopher setPosition:ccp(330, 180)];
    [self.gopher setOpacity:0];
    
    self.gopherSprite = [CCSprite spriteWithSpriteFrameName:@"gopher1.png"];
    [self.gopherSprite setPosition:ccp(330, 180)];
    
    self.peacock = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"peacock1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"peacock1.png"] target:self selector:@selector(peacockTapped)];
    [self.peacock setPosition:ccp(865, 305)];
    [self.peacock setOpacity:0];
    [self.peacock setScale:1.4];
    
    self.peacockSprite = [CCSprite spriteWithSpriteFrameName:@"peacock1.png"];
    [self.peacockSprite setPosition:ccp(865, 305)];
    [self.peacockSprite setScale:1.4];
    
    self.clickableMenu = [CCMenu menuWithItems:self.bluebird, self.gopher, self.peacock, self.lizard, nil];
    self.clickableMenu.position = CGPointZero;
    
    self.ghBoy = [CCSprite spriteWithSpriteFrameName:@"GiantsHill1Boy1.png"];
    [self.ghBoy setPosition:ccp(1024/2, 768/2)];
    self.ghGiant = [CCSprite spriteWithSpriteFrameName:@"GiantsHill2Giant1.png"];
    [self.ghGiant setPosition:ccp(1024/2, 768/2)];
    self.ghWind = [CCSprite spriteWithSpriteFrameName:@"GiantsHill5Wind1.png"];
    [self.ghWind setPosition:ccp(1024/2, 768/2)];
    
    [self addChild:self.clickableMenu z:1];
    [self addChild:self.bluebirdSprite z:1];
    [self addChild:self.gopherSprite z:1];
    [self addChild:self.peacockSprite z:1];
    [self addChild:self.lizardSprite z:1];
    [self addChild:self.ghBoy z:1];
    [self addChild:self.ghGiant z:1];
    [self addChild:self.ghWind z:1];
    
    [super setupInitialFrame];
}

- (void) nextSceneTapped {
    [super nextSceneTapped];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[WizardshouseLayer scene] withColor:ccWHITE]];
    
}

- (void) lizardTapped {
    NSLog(@"lizzardTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    if (lizardcolor == 0) {
        for (int i = 2; i <= 39; i++) {
            [frames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"lizardgreen%d.png", i]]];
        }
    }
    else if (lizardcolor == 1) {
        for (int i = 2; i <= 37; i++) {
            [frames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"lizardred%d.png", i]]];
        }
    }
    else if (lizardcolor == 2) {
        for (int i = 2; i <= 36; i++) {
            [frames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"lizardblue%d.png", i]]];
        }
    }
    else {
        for (int i = 2; i <= 36; i++) {
            [frames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"lizardclear%d.png", i]]];
        }
    }

    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.lizardSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
    
    if (lizardcolor != 3) {
        lizardcolor++;
    }
    else {
        lizardcolor = 0;
    }
}


-(void) bluebirdTapped {
    NSLog(@"bluebirdTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 70; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"bluebird%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.bluebirdSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

-(void) gopherTapped {
    NSLog(@"gopherTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 64; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"gopher%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.gopherSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

-(void) peacockTapped {
    NSLog(@"peacockTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 60; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"peacock%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.peacockSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

- (void) runAnimation: (int) animationNumber {
    
    if (animationNumber == 1) {
        [self.ghBoy runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.boyAnimation1], nil]];
    }
    else if (animationNumber == 2) {
        [self.ghGiant runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.giantAnimation2], nil]];
    }
    else if (animationNumber == 3) {
        [self.ghBoy runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.boyAnimation3], nil]];
        [self.ghGiant runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.giantAnimation3], nil]];
    }
    else if (animationNumber == 4) {
        [self.ghGiant runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.giantAnimation4], nil]];
    }
    else if (animationNumber == 5) {
        [self.ghBoy runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.boyAnimation5], nil]];
        [self.ghGiant runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.giantAnimation5], nil]];
        [self.ghWind runAction:[CCSequence actions:[CCAnimate actionWithAnimation:self.windAnimation5], nil]];
    }
}

@end
