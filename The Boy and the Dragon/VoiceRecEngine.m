//
//  VoiceRecEngine.m
//  The Boy and The Bears
//
//  Created by P Balasubramanian on 4/26/13.
//
//

#import "VoiceRecEngine.h"
#import <OpenEars/OpenEarsLogging.h>

@implementation VoiceRecEngine

@synthesize pocketsphinxController;
@synthesize openEarsEventsObserver;
@synthesize lmGenerator;

@synthesize voiceRecOn;
@synthesize voiceRecStarted;
@synthesize voiceRecSuspended;


static VoiceRecEngine *sharedInstance = nil;

// Get the shared instance and create it if necessary.
+ (VoiceRecEngine *)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    
    if (self) {
        
        if (pocketsphinxController == nil) {
            pocketsphinxController = [[PocketsphinxController alloc] init];
        }
        
        if (openEarsEventsObserver == nil) {
            openEarsEventsObserver = [[OpenEarsEventsObserver alloc] init];
        }
        
        if (lmGenerator == nil)
            lmGenerator = [[LanguageModelGenerator alloc] init];
        
        voiceRecOn = true;
        
        [OpenEarsLogging startOpenEarsLogging];

        pocketsphinxController.secondsOfSilenceToDetect = 0.7;
        pocketsphinxController.verbosePocketSphinx = true;
        
    }
    
    return self;
}

// Your dealloc method will never be called, as the singleton survives for the duration of your app.

// We don't want to allocate a new instance, so return the current one.
+ (id)allocWithZone:(NSZone*)zone {
    return [self sharedInstance];
}

// Equally, we don't want to generate multiple copies of the singleton.
- (id)copyWithZone:(NSZone *)zone {
    return self;
}

// Once again - do nothing, as we don't have a retain counter for this object.
- (id)retain {
    return self;
}

// Replace the retain counter so we can never release this object.
- (NSUInteger)retainCount {
    return NSUIntegerMax;
}

// This function is empty, as we don't want to let the user release this object.
- (oneway void)release {
    
}

//Do nothing, other than return the shared instance - as this is expected from autorelease.
- (id)autorelease {
    return self;
}

@end
