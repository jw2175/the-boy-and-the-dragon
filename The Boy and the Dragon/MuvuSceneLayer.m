//
//  MuvuSceneLayer.m
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 5/31/14.
//  Copyright 2014 Muvu Media. All rights reserved.
//

#import "MuvuSceneLayer.h"
#import "TitleLayer.h"

@implementation MuvuSceneLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MuvuSceneLayer *layer = [MuvuSceneLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}


- (void) setupInitialFrame {
    
    self.nextSentenceButton = [CCMenuItemLabel itemWithLabel:[[CCLabelTTF alloc] initWithString:@"Next Sentence" fontName:@"Century Schoolbook" fontSize:20]
                                                                                  target:self
                                                                                selector:@selector(nextSentenceTapped)];
    self.nextSentenceButton.anchorPoint = ccp(0,0);
    self.nextSentenceButton.position = ccp(450, 10);
    
    self.nextAnimationButton = [CCMenuItemLabel itemWithLabel:[[CCLabelTTF alloc] initWithString:@"Next Animation" fontName:@"Century Schoolbook" fontSize:20]
                                                       target:self
                                                     selector:@selector(nextAnimationTapped)];
    self.nextAnimationButton.position = ccp(650, 10);
        self.nextAnimationButton.anchorPoint = ccp(0,0);
    
    self.nextSceneButton = [CCMenuItemLabel itemWithLabel:[[CCLabelTTF alloc] initWithString:@"Next Scene" fontName:@"Century Schoolbook" fontSize:20]
                                                   target:self
                                                 selector:@selector(nextSceneTapped)];
    self.nextSceneButton.position = ccp(850, 10);
        self.nextSceneButton.anchorPoint = ccp(0,0);
    
    self.pauseButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithFile:@"Pause_Button.png"]
                                              selectedSprite:[CCSprite spriteWithFile:@"Pause_Button.png"]
                                                      target:self
                                                    selector:@selector(pauseButtonTapped)];
    
    self.pauseButton.position = ccp(980, 725);
    
    self.pauseBackground = [CCSprite spriteWithFile:@"Pause_Background.png"];
    self.pauseBackground.anchorPoint = CGPointMake(0, 0);
    [self.pauseBackground setOpacity:0];
    
    self.resumeButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithFile:@"Resume_Off.png"]
                                              selectedSprite:[CCSprite spriteWithFile:@"Resume_On.png"]
                                                      target:self
                                                    selector:@selector(resumeButtonTapped)];
    
    self.resumeButton.position = ccp(512, 500);
    [self.resumeButton setOpacity:0];
    [self.resumeButton setIsEnabled:NO];
    
    self.startoverButton = [CCMenuItemImage itemWithNormalSprite:[CCSprite spriteWithFile:@"StartOver_Off.png"]
                                               selectedSprite:[CCSprite spriteWithFile:@"StartOver_On.png"]
                                                       target:self
                                                     selector:@selector(startoverButtonTapped)];
    
    self.startoverButton.position = ccp(512, 350);
    [self.startoverButton setOpacity:0];
    [self.startoverButton setIsEnabled:NO];
    
    self.mainMenu = [CCMenu menuWithItems:self.nextSentenceButton, self.nextAnimationButton, self.nextSceneButton, self.pauseButton, self.resumeButton, self.startoverButton, nil];
    self.mainMenu.position = CGPointZero;
    
    [self addChild:self.background z:0];
    [self addChild:self.mainMenu z:6];
    [self addChild:self.pauseBackground z:5];
    
    self.sentenceCount = 0;
    self.animationCount = 0;
}

- (void) nextSceneTapped {
    NSLog(@"nextSceneTapped");
    
}


- (void) pauseButtonTapped {
    NSLog(@"pauseButtonTapped");
    [self.pauseBackground setOpacity:255];
    [self.resumeButton setOpacity:255];
    [self.startoverButton setOpacity:255];
    
    [self.resumeButton setIsEnabled:YES];
    [self.startoverButton setIsEnabled:YES];
}

- (void) resumeButtonTapped {
    NSLog(@"resumeButtonTapped");
    
    [self.pauseBackground setOpacity:0];
    [self.resumeButton setOpacity:0];
    [self.startoverButton setOpacity:0];
    
    [self.resumeButton setIsEnabled:NO];
    [self.startoverButton setIsEnabled:NO];
}

- (void) startoverButtonTapped {
    NSLog(@"staroverButtonTapped");
    
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[TitleLayer scene] withColor:ccWHITE]];
    
}

- (void) nextSentenceTapped {
    NSLog(@"nextSentenceTapped");
    if ([self.sentenceBlocks count] == self.sentenceCount) {
        return;
    }

    [self.sentenceMenu1 removeAllChildrenWithCleanup:YES];
    [self.sentenceMenu1 removeFromParentAndCleanup:YES];
    [self.sentenceMenu2 removeAllChildrenWithCleanup:YES];
    [self.sentenceMenu2 removeFromParentAndCleanup:YES];
    [self.sentenceMenu3 removeAllChildrenWithCleanup:YES];
    [self.sentenceMenu3 removeFromParentAndCleanup:YES];
    [self.sentenceMenu4 removeAllChildrenWithCleanup:YES];
    [self.sentenceMenu4 removeFromParentAndCleanup:YES];

    [self displaySentenceBlock:self.sentenceCount];
    
    self.sentenceCount++;
    
}

- (void) nextAnimationTapped {
    NSLog(@"nextAnimationTapped");
    
    self.animationCount++;
    [self runAnimation:self.animationCount];
}

- (void) displaySentenceBlock: (int) sentenceNumber {
    
    NSArray *sentenceBlock = [self.sentenceBlocks objectAtIndex:sentenceNumber];
    NSString *sentence1 = [sentenceBlock objectAtIndex:0];
    NSString *sentence2 = [sentenceBlock objectAtIndex:1];
    NSString *sentence3 = [sentenceBlock objectAtIndex:2];
    NSString *sentence4 = [sentenceBlock objectAtIndex:3];
    
    NSArray *sentenceArray1 = [sentence1 componentsSeparatedByString:@" "];
    NSArray *sentenceArray2 = [sentence2 componentsSeparatedByString:@" "];
    NSArray *sentenceArray3 = [sentence3 componentsSeparatedByString:@" "];
    NSArray *sentenceArray4 = [sentence4 componentsSeparatedByString:@" "];

    self.sentenceMenu1 = [[CCMenu alloc] init];
    self.sentenceMenu2 = [[CCMenu alloc] init];
    self.sentenceMenu3 = [[CCMenu alloc] init];
    self.sentenceMenu4 = [[CCMenu alloc] init];
    
    float wordWidth;
    float sentenceWidth1 = 0;
        float sentenceWidth2 = 0;
        float sentenceWidth3 = 0;
        float sentenceWidth4 = 0;
    
    NSString *word;
    CCMenuItemLabel *prevWordItem;
    for (int i = 0; i < [sentenceArray1 count]; i++) {
        
        word = [sentenceArray1 objectAtIndex:i];
        CCMenuItemLabel *wordItem = [CCMenuItemLabel itemWithLabel:[[CCLabelTTF alloc] initWithString:word fontName:@"Century Schoolbook" fontSize:40]
                                       target:self
                                     selector:@selector(wordTapped:)];
        wordItem.color = ccBLACK;
        [self.sentenceMenu1 addChild:wordItem z:2];
        [wordItem setAnchorPoint:ccp(0,0)];
        if (i == 0) {
            wordItem.position = ccp(0, 0);
        }
        else {
            wordWidth = prevWordItem.label.contentSize.width;
            wordItem.position = ccp(prevWordItem.position.x + wordWidth + 10, 0);
            sentenceWidth1 = sentenceWidth1 + wordWidth + 10;
        }
        prevWordItem = wordItem;
    }
    sentenceWidth1 = sentenceWidth1 + prevWordItem.contentSize.width;
    for (int i = 0; i < [sentenceArray2 count]; i++) {
        
        word = [sentenceArray2 objectAtIndex:i];
        
        CCMenuItemLabel *wordItem = [CCMenuItemLabel itemWithLabel:[[CCLabelTTF alloc] initWithString:word fontName:@"Century Schoolbook" fontSize:40]
                                                            target:self
                                                          selector:@selector(wordTapped:)];
        wordItem.color = ccBLACK;
        [self.sentenceMenu2 addChild:wordItem];
        [wordItem setAnchorPoint:ccp(0,0)];
        if (i == 0) {
            wordItem.position = ccp(0, 0);
        }
        else {
            wordWidth = prevWordItem.label.contentSize.width;
            wordItem.position = ccp(prevWordItem.position.x + wordWidth + 10, 0);
            sentenceWidth2 = sentenceWidth2 + wordWidth + 10;
        }
        prevWordItem = wordItem;
    }
    sentenceWidth2 = sentenceWidth2 + prevWordItem.contentSize.width;
    for (int i = 0; i < [sentenceArray3 count]; i++) {
        
        word = [sentenceArray3 objectAtIndex:i];
        
        CCMenuItemLabel *wordItem = [CCMenuItemLabel itemWithLabel:[[CCLabelTTF alloc] initWithString:word fontName:@"Century Schoolbook" fontSize:40]
                                                            target:self
                                                          selector:@selector(wordTapped:)];
        wordItem.color = ccBLACK;
        [self.sentenceMenu3 addChild:wordItem];
        [wordItem setAnchorPoint:ccp(0,0)];
        if (i == 0) {
            wordItem.position = ccp(0, 0);
        }
        else {
            wordWidth = prevWordItem.label.contentSize.width;
            wordItem.position = ccp(prevWordItem.position.x + wordWidth + 10, 0);
            sentenceWidth3 = sentenceWidth3 + wordWidth + 10;
        }
        prevWordItem = wordItem;
    }
    sentenceWidth3 = sentenceWidth3 + prevWordItem.contentSize.width;
    for (int i = 0; i < [sentenceArray4 count]; i++) {
        
        word = [sentenceArray4 objectAtIndex:i];
        
        CCMenuItemLabel *wordItem = [CCMenuItemLabel itemWithLabel:[[CCLabelTTF alloc] initWithString:word fontName:@"Century Schoolbook" fontSize:40]
                                                            target:self
                                                          selector:@selector(wordTapped:)];
        wordItem.color = ccBLACK;
        [self.sentenceMenu4 addChild:wordItem];
        [wordItem setAnchorPoint:ccp(0,0)];
        if (i == 0) {
            wordItem.position = ccp(0, 0);
        }
        else {
            wordWidth = prevWordItem.label.contentSize.width;
            wordItem.position = ccp(prevWordItem.position.x + wordWidth + 10, 0);
            sentenceWidth4 = sentenceWidth4 + wordWidth + 10;
        }
        prevWordItem = wordItem;
    }
    sentenceWidth4 = sentenceWidth4 + prevWordItem.contentSize.width;
    
    self.sentenceMenu1.position = ccp(512-sentenceWidth1/2, 670);
    self.sentenceMenu2.position = ccp(512-sentenceWidth2/2, 585);
    self.sentenceMenu3.position = ccp(512-sentenceWidth3/2, 500);
    self.sentenceMenu4.position = ccp(512-sentenceWidth4/2, 415);
    
    [self addChild:self.sentenceMenu1 z:4];
    [self addChild:self.sentenceMenu2 z:4];
    [self addChild:self.sentenceMenu3 z:4];
    [self addChild:self.sentenceMenu4 z:4];
    
}


- (void) wordTapped: (id) sender {
    CCMenuItemLabel *word = (CCMenuItemLabel *) sender;
    NSString *str = word.label.string;
    NSLog(@"%@", str);
}

- (void) runAnimation: (int) animationNumber {
    
}

- (void) doubleSize: (NSArray *) spriteArray {
    for (int i = 0; i < [spriteArray count]; i++) {
        ((CCSprite *)[spriteArray objectAtIndex:i]).scale = 2.0;
    }
}

@end
