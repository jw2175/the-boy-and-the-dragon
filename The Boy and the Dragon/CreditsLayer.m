//
//  CreditsLayer.m
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 6/1/14.
//  Copyright 2014 Muvu Media. All rights reserved.
//

#import "CreditsLayer.h"
#import "TitleLayer.h"


@implementation CreditsLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	CreditsLayer *layer = [CreditsLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

- (id)init {
    if( (self=[super init]) ) {
        
        [self setUpInitialFrame];
        
    }
    return self;
}

- (void) setUpInitialFrame {
    self.background = [CCSprite spriteWithFile:@"creditsBG.png"];
    self.background.anchorPoint = CGPointMake(0, 0);
    [super setupInitialFrame];
    
    [self.nextAnimationButton setOpacity:0];
    [self.nextSentenceButton setOpacity:0];
    [self.nextAnimationButton setIsEnabled:NO];
    [self.nextSentenceButton setIsEnabled:NO];

}

- (void) nextSceneTapped {
    [super nextSceneTapped];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[TitleLayer scene] withColor:ccWHITE]];
    
}

@end
