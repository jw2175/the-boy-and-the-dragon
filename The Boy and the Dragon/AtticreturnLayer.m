//
//  AtticreturnLayer.m
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 5/31/14.
//  Copyright 2014 Muvu Media. All rights reserved.
//

#import "AtticreturnLayer.h"
#import "EndLayer.h"


@implementation AtticreturnLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	AtticreturnLayer *layer = [AtticreturnLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

- (id)init {
    if( (self=[super init]) ) {
        
        NSArray *sentenceBlock1 = [NSArray arrayWithObjects:
                                   @"Back in the attic",
                                   @"the boy found his friend",
                                   @"and they ran back downstairs.",
                                   @"The end.",
                                   nil];
        
        self.sentenceBlocks = [NSMutableArray arrayWithObjects:sentenceBlock1, nil];
        
        [self setUpInitialFrame];
        
    }
    return self;
}

- (void) setUpInitialFrame {
    self.background = [CCSprite spriteWithFile:@"atticBG.png"];
    self.background.anchorPoint = CGPointMake(0, 0);
    [super setupInitialFrame];
}

- (void) nextSceneTapped {
    [super nextSceneTapped];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[EndLayer scene] withColor:ccWHITE]];
    
}

@end
