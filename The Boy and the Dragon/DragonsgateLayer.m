//
//  DragonsgateLayer.m
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 5/31/14.
//  Copyright 2014 Muvu Media. All rights reserved.
//

#import "DragonsgateLayer.h"
#import "PrincesstowerLayer.h"

@interface DragonsgateLayer ()

@property (nonatomic, strong) CCSprite *gargoylefatSprite;
@property (nonatomic, strong) CCSprite *gargoyleskinnySprite;
@property (nonatomic, strong) CCSprite *crowSprite;
@property (nonatomic, strong) CCSprite *troutSprite;
@property (nonatomic, strong) CCSprite *torchSprite;

@property (nonatomic, strong) CCMenuItemSprite *gargoylefat;
@property (nonatomic, strong) CCMenuItemSprite *gargoyleskinny;
@property (nonatomic, strong) CCMenuItemSprite *crow;
@property (nonatomic, strong) CCMenuItemSprite *trout;

@end

@implementation DragonsgateLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	DragonsgateLayer *layer = [DragonsgateLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

- (id)init {
    if( (self=[super init]) ) {
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"gargoylefat.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"gargoyleskinny.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"crow.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"trout.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"torch.plist"];
        
        NSArray *sentenceBlock1 = [NSArray arrayWithObjects:
                                   @"At the end of the road,",
                                   @"after walking many hours,",
                                   @"the boy arrived",
                                   @"at the dragon's tower.",
                                   nil];
        NSArray *sentenceBlock2 = [NSArray arrayWithObjects:
                                   @"Then the dragon",
                                   @"turned his back",
                                   @"and waited to see",
                                   @"if the boy would attack.",
                                   nil];
        NSArray *sentenceBlock3 = [NSArray arrayWithObjects:
                                   @"The dragon gave out",
                                   @"a great big roar.",
                                   @"The boy gave the dragon",
                                   @"something he'd never had before.",
                                   nil];
        
        self.sentenceBlocks = [NSMutableArray arrayWithObjects:sentenceBlock1, sentenceBlock2, sentenceBlock3, nil];
        
        [self setUpInitialFrame];
        
    }
    return self;
}

- (void) setUpInitialFrame {
    self.background = [CCSprite spriteWithFile:@"dragongateBG.png"];
    self.background.anchorPoint = CGPointMake(0, 0);
    
    self.trout = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"trout1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"trout1.png"] target:self selector:@selector(troutTapped)];
    [self.trout setPosition:ccp(570, 310)];
    [self.trout setOpacity:0];
    
    self.troutSprite = [CCSprite spriteWithSpriteFrameName:@"trout1.png"];
    [self.troutSprite setPosition:ccp(570, 310)];
    
    self.gargoylefat = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"gargoylefat1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"gargoylefat1.png"] target:self selector:@selector(gargoylefatTapped)];
    [self.gargoylefat setPosition:ccp(818, 584)];
    [self.gargoylefat setOpacity:0];
    
    self.gargoylefatSprite = [CCSprite spriteWithSpriteFrameName:@"gargoylefat1.png"];
    [self.gargoylefatSprite setPosition:ccp(818, 584)];
    
    self.gargoyleskinny = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"gargoyleskinny1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"gargoyleskinny1.png"] target:self selector:@selector(gargoyleskinnyTapped)];
    [self.gargoyleskinny setPosition:ccp(590, 545)];
    [self.gargoyleskinny setOpacity:0];
    
    self.gargoyleskinnySprite = [CCSprite spriteWithSpriteFrameName:@"gargoyleskinny1.png"];
    [self.gargoyleskinnySprite setPosition:ccp(590, 545)];
    
    self.crow = [CCMenuItemSprite itemWithNormalSprite:[CCSprite spriteWithSpriteFrameName:@"crow1.png"] selectedSprite:[CCSprite spriteWithSpriteFrameName:@"crow1.png"] target:self selector:@selector(crowTapped)];
    [self.crow setPosition:ccp(870, 270)];
    [self.crow setOpacity:0];
    
    self.crowSprite = [CCSprite spriteWithSpriteFrameName:@"crow1.png"];
    [self.crowSprite setPosition:ccp(870, 240)];
    
    self.torchSprite = [CCSprite spriteWithSpriteFrameName:@"torch1.png"];
    [self.torchSprite setPosition:ccp(400, 400)];
    
    self.clickableMenu = [CCMenu menuWithItems:self.gargoylefat, self.gargoyleskinny, self.crow, self.trout, nil];
    self.clickableMenu.position = CGPointZero;
    
    [self addChild:self.clickableMenu z:1];
    [self addChild:self.gargoylefatSprite z:1];
    [self addChild:self.gargoyleskinnySprite z:1];
    [self addChild:self.crowSprite z:1];
    [self addChild:self.troutSprite z:1];
    [self addChild:self.torchSprite z:1];
    
    [super setupInitialFrame];
    
    [self animatetorch];
}

- (void) nextSceneTapped {
    [super nextSceneTapped];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[PrincesstowerLayer scene] withColor:ccWHITE]];
    
}

-(void) animatetorch {
    NSLog(@"animating torch");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 77; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"torch%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.torchSprite runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:animation]]];
}

-(void) gargoylefatTapped {
    NSLog(@"gargoylefatTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 38; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"gargoylefat%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.gargoylefatSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

-(void) troutTapped {
    NSLog(@"troutTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 45; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"trout%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.troutSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

-(void) gargoyleskinnyTapped {
    NSLog(@"gargoyleskinnyTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 48; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"gargoyleskinny%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.gargoyleskinnySprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

-(void) crowTapped {
    NSLog(@"crowTapped");
    
    NSMutableArray *frames = [NSMutableArray array];
    
    for (int i = 2; i <= 56; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"crow%d.png", i]]];
    }
    
    CCAnimation *animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    [self.crowSprite runAction:[CCSequence actions:[CCAnimate actionWithAnimation:animation], nil]];
}

@end
