//
//  main.m
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 5/27/14.
//  Copyright Muvu Media 2014. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"AppController");
    [pool release];
    return retVal;
}
