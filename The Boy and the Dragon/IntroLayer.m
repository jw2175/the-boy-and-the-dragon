//
//  IntroLayer.m
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 5/27/14.
//  Copyright Muvu Media 2014. All rights reserved.
//


// Import the interfaces
#import "IntroLayer.h"
#import "TitleLayer.h"
#import "AtticLayer.h"


#pragma mark - IntroLayer

// HelloWorldLayer implementation
@implementation IntroLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	IntroLayer *layer = [IntroLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(void) onEnter
{
    [super onEnter];
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA4444];
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"Muvu_Logo.plist"];
    CCSprite *background = [[CCSprite alloc] initWithSpriteFrameName:@"Muvu_Logo_1.png"];
    CCSprite *whiteBackground = [[CCSprite alloc] initWithSpriteFrameName:@"White background.png"];
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0))
    {
        background.scale = 2.0;
        whiteBackground.scale = 2.0;
    }
    
    NSMutableArray *frames = [NSMutableArray arrayWithCapacity:92];
    for (int i = 2; i <= 92; i++) {
        [frames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"Muvu_Logo_%d.png",i]]];
    }
    
    self.animation = [CCAnimation animationWithSpriteFrames:frames delay:0.05f];
    
    background.position = ccp(winSize.width * 0.5, winSize.height * 0.5);
    whiteBackground.position = ccp(winSize.width * 0.5, winSize.height * 0.5);
    
    [self addChild:background z:2];
    [self addChild:whiteBackground z:1];
    [background runAction:[CCAnimate actionWithAnimation:self.animation]];
    
    self.animationSound = [[AudioPlayback alloc] init];
    [self.animationSound PlaySoundFile:@"Muvu Logo Sound Effect" AtTime:.5];
    
    [self scheduleOnce:@selector(makeTransition:) delay:4];
    
}

-(void) makeTransition:(ccTime)dt
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[TitleLayer scene] withColor:ccWHITE]];
}

@end
