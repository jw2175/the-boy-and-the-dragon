//
//  IntroLayer.h
//  The Boy and the Dragon
//
//  Created by Jeffrey Wang on 5/27/14.
//  Copyright Muvu Media 2014. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "AudioPlayback.h"

// HelloWorldLayer
@interface IntroLayer : CCLayer
{
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@property (nonatomic, strong) AudioPlayback *animationSound;
@property (nonatomic, strong) CCAnimation *animation;

@end
