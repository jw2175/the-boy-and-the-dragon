//
//  AudioPlayback.h
//  The Boy and The Bears
//
//  Created by P Balasubramanian on 4/24/13.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "AVFoundation/AVAudioPlayer.h"
#import <AVFoundation/AVFoundation.h>


@interface AudioPlayback : NSObject <AVAudioPlayerDelegate> {
}

-(void)PlaySoundFile:(NSString *) sound_file_name;

-(void) PlaySoundFile:(NSString*) sound_file_name AtTime: (NSTimeInterval)interval;
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag;
@end
